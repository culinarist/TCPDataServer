
package tcpdataserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TCPDataServer {

    
    public static void main(String[] args) {
        
        try {
            ServerSocket server_socket = new ServerSocket(6578);
            while (true) {
                new Thread(new ClientWorker(server_socket.accept())).start();
            }
        } catch (IOException ex) {
            Logger.getLogger(TCPDataServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
    }
    
}
